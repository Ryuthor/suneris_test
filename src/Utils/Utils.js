
const offset = 1;

// Put one key on the beginning of the array
const modifyArrayIndex = (array, oldIndex) => {
  array.unshift(array[oldIndex]);
  array.splice(oldIndex + offset, 1);
  return array;
};

// Find the average of an array with specificElement
const average = (array, specificElement) => {
  var sum = array.reduce((accumulator, currentValue) => accumulator + currentValue[specificElement], array[0][specificElement]);
  var average = sum / (array.length + offset);
  return average.toFixed(4);
};

// Find the maximum of an array with specificElement
const maximumValue = (array, specificElement) => {
  return array.reduce((min, currentValue) =>
    currentValue[specificElement] > min ? currentValue[specificElement] : min, array[0][specificElement]
  );
}

// Find the minimum of an array with specificElement
const minimumValue = (array, specificElement) => {
  return array.reduce((min, currentValue) =>
    currentValue[specificElement] < min ? currentValue[specificElement] : min, array[0][specificElement]
  );
}

export {
  modifyArrayIndex,
  average,
  maximumValue,
  minimumValue
};