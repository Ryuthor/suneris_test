import React from 'react';
import './RenderRow.css';

const RenderRow = (props) => {
  return props.keys.map((key, index) => {
    return <td key={index}>{props.data[key] !== undefined ? props.data[key] : "-"}</td>
  })
}

export default RenderRow;