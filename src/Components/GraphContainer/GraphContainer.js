import React, { Component } from 'react';
import "./GraphContainer.css";
import PropTypes from 'prop-types';
import TemplateGraph from './TemplateGraph/TemplateGraph';
import { maximumValue, minimumValue, average } from '../../Utils/Utils';
import { LocalizedString as strings } from '../../Utils/LocalizedString';

class GraphContainer extends Component {

  switchTest = (props) => {
    const label = strings.label;
    const fields = strings.fields;
    let labelData, dataY;
    let result = [];
    switch (props.activeComponent) {
      case 1:
        labelData = [label.recv, label.send];
        dataY = [fields.recv, fields.send];
        break;
      case 2:
        labelData = [label.used, label.buff, label.cach, label.free];
        dataY = [fields.used, fields.buff, fields.cach, fields.free];
        break;
      case 3:
        labelData = [label.usr, label.sys, label.idl, label.wai, label.hiq, label.siq];
        dataY = [fields.usr, fields.sys, fields.idl, fields.wai, fields.hiq, fields.siq];
        break;
      case 4:
        labelData = [label.read, label.writ];
        dataY = [fields.read, fields.writ];
        break;
      case 5:
        labelData = [label._1m, label._5m, label._15m];
        dataY = [fields._1m, fields._5m, fields._15m];
        break;
      default:
        return null;
    }

    for (let index = 0; index < labelData.length; index++) {

      result.push(<TemplateGraph
        key={index}
        data={props.data}

        xLabel={props.xLabel}
        yLabel={labelData[index]}

        positionXLabel={props.positionXLabel}
        positionYLabel={props.positionYLabel}

        dataXAxis={props.dataXAxis}
        dataYAxis={dataY[index]}

        minimumValue={minimumValue(props.data, dataY[index])}
        maximumValue={maximumValue(props.data, dataY[index])}
        averageValue={average(props.data, dataY[index])}
      />);

    }
    return result;
  }

  render() {
    var props = this.props;
    return (
      <div className="graphs-Container" >
        {this.switchTest(props)}
      </div>
    );
  }
}

GraphContainer.propTypes = {
  data: PropTypes.array.isRequired,
  xLabel: PropTypes.string.isRequired,
  dataXAxis: PropTypes.string.isRequired,
  positionXLabel: PropTypes.string.isRequired,
  positionYLabel: PropTypes.string.isRequired,
  activeComponent: PropTypes.number,
};

export default GraphContainer;