import React from 'react';
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  ResponsiveContainer,
  Tooltip,
  Label,
  Brush,
  ReferenceLine
} from 'recharts';
import PropTypes from 'prop-types';

const TemplateGraph = props => {
  return (
    <ResponsiveContainer
      className="graph"
      minWidth={750}
      width='45%'
      height={500}
    >
      <LineChart
        margin={{ top: 5, right: 30, left: 20, bottom: 50 }}
        data={props.data}
      >
        <CartesianGrid stroke="#ddd" />
        <Tooltip />

        <XAxis dataKey={props.dataXAxis}>
          <Label value={props.xLabel} position={props.positionXLabel} />
        </XAxis>

        <YAxis width={150} domain={['dataMin', 'dataMax']}>
          <Label value={props.yLabel} angle={-90} position={props.positionYLabel} />
        </YAxis>

        <ReferenceLine y={props.minimumValue} stroke="green" label={props.minimumValue} strokeDasharray="3 3" />
        <ReferenceLine y={props.maximumValue} stroke="red" label={props.maximumValue} strokeDasharray="3 3" />
        <ReferenceLine y={props.averageValue} stroke="orange" label={props.averageValue} ifOverflow="extendDomain" />

        <Line dataKey={props.dataYAxis} stroke="#338095" />

        <Brush
          dataKey={props.dataXAxis}
          height={20}
          stroke="#000000"
          y={460}
          startIndex={0}
          endIndex={50}>

          <LineChart>
            <Line dataKey={props.dataYAxis} fill="#338095" />
          </LineChart>

        </Brush>
      </LineChart>
    </ResponsiveContainer>

  );
}

TemplateGraph.propType = {
  data: PropTypes.array.isRequired,
  dataYAxis: PropTypes.string.isRequired,
  dataXAxis: PropTypes.string.isRequired,
  positionXLabel: PropTypes.string.isRequired,
  positionYLabel: PropTypes.string.isRequired,
  maximumValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  minimumValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  averageValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ])
};

export default TemplateGraph;
