import React, { Component } from 'react';
import './TopNavBar.css';
import { LocalizedString as strings } from '../../Utils/LocalizedString';
import PropTypes from 'prop-types';

class TopNavBar extends Component {

  render() {
    const props = this.props;
    return (
      <nav className="menu-container" >
        <ul className="navigation">
          <li>
            <button onClick={() => props.onClickNavItem(0, strings.title.table)}>
              {strings.title.table}
            </button>
          </li>
          <li>
            <button onClick={() => props.onClickNavItem(1, strings.title.network)}>
              {strings.title.network}
            </button>
          </li>
          <li>
            <button onClick={() => props.onClickNavItem(2, strings.title.memory)}>
              {strings.title.memory}
            </button>
          </li>
          <li>
            <button onClick={() => props.onClickNavItem(3, strings.title.cpu)}>
              {strings.title.cpu}
            </button>
          </li>
          <li>
            <button onClick={() => props.onClickNavItem(4, strings.title.disk)}>
              {strings.title.disk}
            </button>
          </li>
          <li>
            <button onClick={() => props.onClickNavItem(5, strings.title.loadAverage)}>
              {strings.title.loadAverage}
            </button>
          </li>
        </ul>
      </nav>
    );
  }
}

TopNavBar.propTypes = {
  onClickNavItem: PropTypes.func,
};

export default TopNavBar;